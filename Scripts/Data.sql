USE [ONELINK_BPO_DB]
GO
SET IDENTITY_INSERT [dbo].[Autenticacion] ON 

INSERT [dbo].[Autenticacion] ([ID], [Usuario], [Contrasena], [Activo]) VALUES (1, N'admin', N'k3kIIN+XUWnwYZ7QCq2b5g==', 1) -- clave: dominic
SET IDENTITY_INSERT [dbo].[Autenticacion] OFF
GO
SET IDENTITY_INSERT [dbo].[TiposDocumentos] ON 

INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (1, N'CC', N'CÉDULA DE CIUDADANÍA')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (2, N'CE', N'CÉDULA EXTRANJERÍA')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (3, N'CI', N'CARNET DE IDENTIDAD')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (4, N'DNI', N'DOCUMENTO NACIONAL DE IDENTIDAD')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (5, N'DUI', N'DOCUMENTO ÚNICO DE IDENTIDAD')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (6, N'RC', N'REGISTRO CIVIL')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (7, N'TI', N'TARJETA DE IDENTIDAD')
INSERT [dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES (8, N'TP', N'TARJETA PASAPORTE')
SET IDENTITY_INSERT [dbo].[TiposDocumentos] OFF
GO
SET IDENTITY_INSERT [dbo].[Areas] ON 

INSERT [dbo].[Areas] ([ID], [Nombre]) VALUES (1, N'ADMINISTRACIÓN')
INSERT [dbo].[Areas] ([ID], [Nombre]) VALUES (2, N'INGENIERÍA')
INSERT [dbo].[Areas] ([ID], [Nombre]) VALUES (3, N'INVESTIGACIÓN + DESARROLLO')
INSERT [dbo].[Areas] ([ID], [Nombre]) VALUES (4, N'TALENTO HUMANO')
SET IDENTITY_INSERT [dbo].[Areas] OFF
GO
SET IDENTITY_INSERT [dbo].[Subareas] ON 

INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (1, N'CONTABILIDAD', 1)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (2, N'SEGURIDAD Y SALUD EN EL TRABAJO', 1)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (3, N'SOPORTE TÉCNICO', 2)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (4, N'SEGURIDAD INFORMÁTICA', 2)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (5, N'CALIDAD', 2)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (6, N'INNOVACIÓN', 3)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (7, N'DESARROLLO', 3)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (8, N'BI', 3)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (9, N'BIENESTAR', 4)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (10, N'RECLUTAMIENTO', 4)
INSERT [dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES (11, N'PSICOLOGÍA', 4)
SET IDENTITY_INSERT [dbo].[Subareas] OFF
GO
SET IDENTITY_INSERT [dbo].[Empleados] ON 

INSERT [dbo].[Empleados] ([ID], [TipoDocumentoID], [NumeroDocumento], [Nombres], [Apellidos], [SubareaID]) VALUES (1, 1, N'10502225050', N'Raúl Alejandro', N'Rodríguez Muñoz', 2)
SET IDENTITY_INSERT [dbo].[Empleados] OFF
GO
SET IDENTITY_INSERT [dbo].[TipoRol] ON 

INSERT [dbo].[TipoRol] ([ID], [EmpleadoID], [AutenticacionID]) VALUES (1, 1, 1)
SET IDENTITY_INSERT [dbo].[TipoRol] OFF
GO
