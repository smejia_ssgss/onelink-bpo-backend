﻿using System;
using Microsoft.AspNetCore.Http;

namespace App.Common.Extensions
{
    public static class HeaderRequestExtension
    {
        public static bool CheckAndGetValue<T>(this IHeaderDictionary headers, string headerName, out T year)
        {
            year = default;
            if (headers.ContainsKey(headerName))
            {
                var typeValue = typeof(T);
                string value = headers[headerName];

                if (typeValue.IsGenericType && typeValue.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    if (value == null)
                    {
                        year = default;
                        return true;
                    }

                    typeValue = Nullable.GetUnderlyingType(typeValue);
                }

                year = (T)Convert.ChangeType(value, typeValue);
                return true;
            }

            return false;
        }

        public static T GetValue<T>(this IHeaderDictionary headers, string headerName)
        {
            var year = default(T);

            if (headers.ContainsKey(headerName))
            {
                var typeValue = typeof(T);
                string value = headers[headerName];

                if (typeValue.IsGenericType && typeValue.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    if (value == null)
                    {
                        return default;
                    }

                    typeValue = Nullable.GetUnderlyingType(typeValue);
                }

                year = (T)Convert.ChangeType(value, typeValue);
            }

            return year;
        }
    }
}
