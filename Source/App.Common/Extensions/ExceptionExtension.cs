﻿using App.Common.Classes.Constants;
using App.Common.Classes.Exceptions;
using System;
using System.Text;

namespace App.Common.Extensions
{
    public static class ExceptionExtension
    {
        public static string ToUlHtmlString(this Exception ex)
        {
            Type type = ex.GetType();

            StringBuilder sb = new StringBuilder();
            sb.Append(ExtensionConstants.OPEN_LIST);

            switch (type.FullName)
            {
                case ExtensionConstants.ERROR_VALIDATION_SERVICE_EXCEPTION:

                    ValidationServiceException valService =
                        ex as ValidationServiceException;

                    foreach (var item in valService.ErrorMessages)
                    {
                        sb.Append(string.Format(ExtensionConstants.LIST_ITEM,
                            item));
                    }
                    break;

                case ExtensionConstants.ERROR_APPLICACTION_EXCEPTION:
                    sb.Append(string.Format(ExtensionConstants.LIST_ITEM,
                            ex.Message));
                    break;

                default:
                    sb.Append(ExtensionConstants.GENERIC_ERROR);
                    break;
            }

            sb.Append(ExtensionConstants.CLOSE_LIST);

            return sb.ToString();

        }
    }
}
