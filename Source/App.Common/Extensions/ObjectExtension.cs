﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace App.Common.Extensions
{
    public static class ObjectExtension
    {
        public static object GetPropertyValue(this object entity,
            string propertyName)
        {
            var entityProperty = entity.GetType().GetProperties()
                .FirstOrDefault(c => c.Name == propertyName);

            if (entityProperty != null)
            {
                return entityProperty.GetValue(entity, null);
            }

            return null;
        }

        public static object GetKeyValue(this object entity)
        {
            var entityKey = entity.GetType().GetProperties()
                .FirstOrDefault(c => c.CustomAttributes
                .Any(attr => attr.AttributeType == typeof(KeyAttribute)));

            if (entityKey != null)
            {
                return entityKey.GetValue(entity, null);
            }

            return null;
        }

        public static object SetPropertyValue(this object entity,
            string propertyName, object value)
        {
            var entityProperty = entity.GetType().GetProperties()
                .FirstOrDefault(c => c.Name == propertyName);

            if (entityProperty != null)
            {
                var propertyType = entityProperty.PropertyType;

                var targetType = IsNullableType(entityProperty.PropertyType)
                    ? Nullable.GetUnderlyingType(entityProperty.PropertyType)
                    : entityProperty.PropertyType;

                value = Convert.ChangeType(value, targetType);
                entityProperty.SetValue(entity, value, null);
            }

            return entity;
        }

        public static bool IsNullableType(Type type)
        {
            return type.IsGenericType
                && type.GetGenericTypeDefinition()
                .Equals(typeof(Nullable<>));
        }
    }
}
