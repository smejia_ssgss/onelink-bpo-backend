﻿using FluentValidation.Validators;

namespace App.Common.Classes.Base.Validator.FluentValidator
{
    public class NumericValidator : RegularExpressionValidator
    {
        public NumericValidator() : base("^[0-9]*$") { }
    }
}
