﻿using App.Common.Classes.Base.Services;
using App.Common.Classes.DTO.Common;
using App.Common.Classes.DTO.Request;
using App.Common.Classes.Exceptions;
using App.Common.Extensions;
using App.Common.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Rollbar;
using System;
using System.Net;
using System.Threading.Tasks;

namespace App.Common.Classes.Base.WebApi
{
    public abstract class BaseController<TDTO> : Controller
        where TDTO : class
    {
        IStringLocalizer<GlobalResource> _globalLocalizer;
        IBaseService<TDTO> _service;

        public BaseController(IBaseService<TDTO> service, IStringLocalizer<GlobalResource> globalLocalizer)
        {
            _service = service;
            _globalLocalizer = globalLocalizer;
        }

        public BaseController() { }

        #region GetAll

        [HttpGet, Route("")]
        public IActionResult Get()
        {
            return Ok(_service.GetAll());
        }

        [HttpGet]
        [Route("getAll")]
        public virtual async Task<IActionResult> Index()
        {
            try
            {
                var data = await _service.GetAllAsync();
                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["DefaultError"]));
            }
        }

        [HttpPost]
        [Route("getAll")]
        public virtual async Task<IActionResult> Index([FromBody] PagingParamDTO pagingParams)
        {
            try
            {
                var data = await _service.GetAllPagedAsync(pagingParams);

                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["DefaultError"]));
            }
        }

        #endregion

        #region FindById

        [HttpGet, Route("{id}")]
        public IActionResult GetByID(int id)
        {
            return Ok(_service.FindById(id));
        }

        [HttpGet]
        [Route("details/{id:int}")]
        public virtual async Task<IActionResult> Details(int id)
        {
            try
            {
                var data = await _service.FindByIdAsync(id);

                if (data == null)
                    return Json(ResponseExtension.AsResponseDTO<string>(null,
                        (int)HttpStatusCode.NotAcceptable,
                        _globalLocalizer["ItemNotFoundMessage"]));

                return Json(data.AsResponseDTO((int)HttpStatusCode.OK));
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["DefaultError"]));
            }
        }

        #endregion

        #region Create

        [HttpPost, Route("save")]
        public IActionResult Post([FromBody]TDTO modelDTO)
        {
            return Ok(_service.Create(modelDTO));
        }

        [HttpPost]
        [Route("create")]
        public virtual async Task<IActionResult> Create([FromBody]TDTO modelDTO)
        {
            try
            {
                var data = await _service.CreateAsync(modelDTO);
                return Json(data.AsResponseDTO((int)HttpStatusCode.OK,
                    _globalLocalizer["CreateSuccessMessage"]));
            }
            catch (ValidationServiceException ex)
            {
                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.NotAcceptable,
                    ex.ToUlHtmlString()));
            }
            catch (ApplicationException ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    ex.ToUlHtmlString()));
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["DefaultError"]));
            }
        }

        #endregion

        #region Update

        [HttpPut, Route("update")]
        public IActionResult Put([FromBody]TDTO modelDTO)
        {
            return Ok(_service.Update(modelDTO));
        }

        [HttpPost]
        [Route("edit")]
        public virtual async Task<IActionResult> Edit([FromBody]TDTO modelDTO)
        {
            try
            {
                int? id = (int?)modelDTO.GetPropertyValue("Id");
                if (!id.HasValue)
                {
                    return Json(ResponseExtension.AsResponseDTO<string>(null,
                      (int)HttpStatusCode.NotAcceptable,
                      _globalLocalizer["ItemNotFoundMessage"]));
                }

                var data = await _service.FindByIdAsync(id);
                if (data == null)
                {
                    return Json(ResponseExtension.AsResponseDTO<string>(null,
                        (int)HttpStatusCode.NotAcceptable,
                        _globalLocalizer["ItemNotFoundMessage"]));
                }

                var result = await _service.UpdateAsync(modelDTO);

                return Json(result.AsResponseDTO((int)HttpStatusCode.OK,
                    _globalLocalizer["UpdateSuccessMessage"]));
            }
            catch (ValidationServiceException ex)
            {
                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.NotAcceptable, ex.ToUlHtmlString()));
            }
            catch (ApplicationException ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["UpdateErrorMessage"]));
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["DefaultError"]));
            }
        }

        #endregion

        #region Delete

        [HttpDelete, Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Ok();
        }

        [HttpPost]
        [Route("delete")]
        public virtual async Task<IActionResult> Delete([FromBody]RequestDTO request)
        {
            try
            {
                var data = await _service.FindByIdAsync(request.Id);

                if (data == null)
                {
                    return Json(ResponseExtension.AsResponseDTO<string>(null,
                        (int)HttpStatusCode.NotAcceptable,
                        _globalLocalizer["ItemNotFoundMessage"]));
                }

                await _service.DeleteAsync(request.Id);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.OK,
                    _globalLocalizer["DeleteSuccessMessage"]));
            }
            catch (ValidationServiceException ex)
            {
                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.NotAcceptable, ex.ToUlHtmlString()));
            }
            catch (ApplicationException ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    ex.ToUlHtmlString()));
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);

                return Json(ResponseExtension.AsResponseDTO<string>(null,
                    (int)HttpStatusCode.InternalServerError,
                    _globalLocalizer["DefaultError"]));
            }
        }

        #endregion
    }
}
