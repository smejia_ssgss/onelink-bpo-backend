﻿using App.Common.Classes.DTO.Request;
using App.Common.Tools.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace App.Common.Classes.Base.Repositories
{
    public interface IReadRepository<T> where T : class
    {
        Task<T> FindByIdAsync(object id);
        T FindById(object id);
        IQueryable<T> GetAll();
        IQueryable<T> GetAllPaging(int pageIndex, int pageSize);
        PagedList<T> GetAllPaged(PagingParamDTO pagingParams);
    }
}
