﻿using App.Common.Classes.DTO.Subarea;
using App.Common.Classes.DTO.TipoDocumento;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Common.Classes.DTO.Empleado
{
    public class EmpleadoDTO
    {
        public int Id { get; set; }

        [DisplayName("Número Documento")]
        [Required(ErrorMessage = "El número de documento es requerido")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Documento Incorrecto")]
        public string NumeroDocumento { get; set; }

        [DisplayName("Tipo Documento")]
        [Required(ErrorMessage = "El tipo de documento es requerido")]
        public int? TipoDocumentoId { get; set; }

        [DisplayName("Nombres")]
        [Required(ErrorMessage = "Los nombres son requeridos")]
        public string Nombres { get; set; }

        [DisplayName("Apellidos")]
        [Required(ErrorMessage = "Los apellidos son requeridos")]
        public string Apellidos { get; set; }

        [DisplayName("Subarea")]
        [Required(ErrorMessage = "La subarea es requerida")]
        public int? SubareaId { get; set; }

        public SubareaDTO Subareas { get; set; }
        public TipoDocumentoDTO TipoDocumentos { get; set; }
    }
}
