﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Common.Classes.DTO.Authentications
{
    public class UsuarioLoginDTO
    {
        [Required(ErrorMessage = "El usuario es obligatorio")]
        [DisplayName("Usuario")]
        public string Us { get; set; }

        [Required(ErrorMessage = "La contraseña es obligatoria")]
        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        public string Psw { get; set; }
    }
}
