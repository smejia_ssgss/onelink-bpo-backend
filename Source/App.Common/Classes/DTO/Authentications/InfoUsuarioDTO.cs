﻿namespace App.Common.Classes.DTO.Authentications
{
    public class InfoUsuarioDTO
    {
        public bool Logged { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
        public int Id { get; set; }
        public string UserName { get; set; }
        public int? EmpleadoId { get; set; }
    }
}
