﻿using App.Common.Classes.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace App.Common.Classes.DTO.Authentications
{
    public class UsuarioDTO
    {
        public int Id { get; set; }

        [DisplayName("Usuario")]
        public string Username { get; set; }

        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
