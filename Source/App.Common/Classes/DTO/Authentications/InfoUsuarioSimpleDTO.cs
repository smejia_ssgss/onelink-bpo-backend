﻿namespace App.Common.Classes.DTO.Authentications
{
    public class InfoUsuarioSimpleDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
