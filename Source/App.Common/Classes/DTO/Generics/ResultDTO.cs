﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.DTO.Generics
{
    public class ResultDTO
    {
        public bool Successful { get; set; }
        public string Message { get; set; }

        public ResultDTO()
        {
            Successful = false;
            Message = "";
        }
    }
}
