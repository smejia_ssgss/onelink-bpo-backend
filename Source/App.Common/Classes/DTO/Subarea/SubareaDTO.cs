﻿using App.Common.Classes.DTO.Area;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Common.Classes.DTO.Subarea
{
    public class SubareaDTO
    {
        public int Id { get; set; }

        [Required, DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Área")]
        [Required(ErrorMessage = "La área es requerida")]
        public int? AreaId { get; set; }

        public AreaDTO Areas { get; set; }
    }
}
