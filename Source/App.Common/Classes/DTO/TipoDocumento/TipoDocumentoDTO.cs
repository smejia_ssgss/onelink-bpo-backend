﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Common.Classes.DTO.TipoDocumento
{
    public class TipoDocumentoDTO
    {
        public int Id { get; set; }

        [Required, DisplayName("Código")]
        public string Codigo { get; set; }

        [Required, DisplayName("Nombre")]
        public string Nombre { get; set; }
    }
}
