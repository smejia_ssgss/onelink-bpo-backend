﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.DTO.Request
{
    public class RequestIdDTO
    {
        public int Id { get; set; }
    }
}
