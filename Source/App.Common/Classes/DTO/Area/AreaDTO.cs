﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Common.Classes.DTO.Area
{
    public class AreaDTO
    {
        public int Id { get; set; }

        [Required, DisplayName("Nombre")]
        public string Nombre { get; set; }
    }
}
