﻿namespace App.Common.Classes.Constants
{
    public class AuthenticationConstants
    {
        public const string SectionName = "JwtConfigurations";
        public const string Issuer = "Issuer";
        public const string Audience = "Audience";
        public const string Expiration = "Expiration";
        public const string NotBefore = "NotBefore";
        public const string Key = "Key";
        public const string Fingerprint = "onelinkprint";
        public const string FingerprintClaim = "upt";
    }
}
