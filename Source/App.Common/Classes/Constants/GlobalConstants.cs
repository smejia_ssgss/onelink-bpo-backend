﻿namespace App.Common.Classes.Constants
{
    public class GlobalConstants
    {
        #region Web Client

        public const string WEB_CLIENT_EXCEPTION = "No podemos procesar la petición, inténtelo más tarde.";

        public const string WEB_CLIENT_UNAUTHORIZED = "Token no valido para hacer la petición";

        public const string WEB_CLIENT_FORBIDDEN = "Token no permitido para hacer la petición";

        #endregion

        #region Error Messages

        public const string ERROR_BASE_MODEL_ONLY_ONE_KEY = "El objeto tiene más de 1 llave primaria. Lo cual no permite la automatización de BaseModel en el Update";

        public const string ERROR_BASE_MODEL_PRIMARY_KEY = "El objeto no tiene llave primaria. Lo cual no permite la automatización de BaseModel en el Update";

        public const string OPERATION_PROCESS_OK = "Operación elaborada Con Éxito";

        #endregion

        #region Routes

        public const string ROUTE_GET_ALL = "api/{0}/getAll";

        public const string ROUTE_FIND_BY_ID = "api/{0}/details/{1}";

        public const string ROUTE_CREATE = "api/{0}/create";

        public const string ROUTE_EDIT = "api/{0}/edit";

        public const string ROUTE_DELETE = "api/{0}/delete";

        #endregion

        #region Filter

        public const string FORMAT_ANY = "any";

        public const string FORMAT_STRING = "string";

        public const string FORMAT_NUMERIC = "numeric";

        public const string FORMAT_DATE = "date";

        public const string FORMAT_BOOLEAN = "boolean";

        public const string CONDITION_CONTAINS = "Contiene";

        public const string CONDITION_GREATER_THAN = "Mayor a";

        public const string CONDITION_GREATER_THAN_OR_EQUAL = "Mayor o igual a";

        public const string CONDITION_LESS_THAN = "Menor a";

        public const string CONDITION_LESS_THAN_OR_EQUAL_TO = "Menor o igual a";

        public const string CONDITION_STARTS_WITH = "Inicia con";

        public const string CONDITION_ENDS_WITH = "Termia con";

        public const string CONDITION_EQUALS = "Igual a";

        public const string CONDITION_NOT_EQUAL = "Diferente a";

        public const string DTO_NAMESPACE = "App.Common.Classes.DTO.";

        #endregion

        #region Fluent Validation

        public const string FLUENT_FIELD_NOT_NULL = "FieldNotNull";

        public const string FLUENT_FIELD_ID = "IdFieldNotNull";

        public const string FLUENT_FIELD_USER_ID = "UserIdFieldNotNull";

        public const string FLUENT_FIELD_CREATED_AT = "CreatedAtFieldNotNull";

        public const string FLUENT_FIELD_UPDATED_AT = "UpdatedAtFieldNotNull";

        #endregion

        public const string KEY = "8B17BD5FFDDBC62B4CDC65D4CC00EBB8673058307EF6841D7A41C40BE8108303";

        public const string IV = "C725A5BFADBC3C9BBFB018C097A7C82F85A3FFC1B9206ABEEBFE79FE9649F477";
    }
}
