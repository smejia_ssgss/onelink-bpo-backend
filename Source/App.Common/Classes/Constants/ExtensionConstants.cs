﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Classes.Constants
{
    public class ExtensionConstants
    {
        #region List

        public const string OPEN_LIST = "<ul>";

        public const string CLOSE_LIST = "</ul>";

        public const string LIST_ITEM = "<li>{0}</li>";

        public const string GENERIC_ERROR = "<li>Ha ocurrido un error, por favor intentarlo más tarde</li>";

        #endregion

        #region Errors

        public const string ERROR_APPLICACTION_EXCEPTION = "System.ApplicationException";

        public const string ERROR_VALIDATION_SERVICE_EXCEPTION = "App.Common.Classes.Exceptions.ValidationServiceException";

        #endregion

        #region Types

        public const string ORDER_TYPE_ASCENDANT = "asc";

        public const string ORDER_TYPE_DESCENDING = "desc";

        #endregion
    }
}
