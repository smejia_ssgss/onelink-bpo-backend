﻿using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;

namespace App.Common.Classes.Cache
{
    public interface IMemoryCacheManager :
        IEnumerable<KeyValuePair<object, object>>, IMemoryCache
    {
        void Clear();
    }
}
