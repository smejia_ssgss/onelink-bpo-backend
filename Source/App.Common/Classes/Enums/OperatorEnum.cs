﻿namespace App.Common.Classes.Enums
{
    public enum FilterOperatorEnum
    {
        Contains,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqualTo,
        Equals
    }
}
