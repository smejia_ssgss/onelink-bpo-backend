﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using App.Common.Classes.Constants;

namespace App.Common.Tools.Security
{
    public class Cryptography
    {
        public static string Encrypt(string text)
        {
            byte[] cipherMessageBytes;
            byte[] key = UTF8Encoding.UTF8.GetBytes(GlobalConstants.KEY);
            byte[] iv = UTF8Encoding.UTF8.GetBytes(GlobalConstants.IV);
            Array.Resize(ref key, 32);
            Array.Resize(ref iv, 16);

            Rijndael RijndaelAlg = Rijndael.Create();

            using (MemoryStream memoryStream = new MemoryStream())
            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, RijndaelAlg.CreateEncryptor(key, iv), CryptoStreamMode.Write))
            {
                byte[] txtPlanoBytes = UTF8Encoding.UTF8.GetBytes(text);

                cryptoStream.Write(txtPlanoBytes, 0, txtPlanoBytes.Length);
                cryptoStream.FlushFinalBlock();

                cipherMessageBytes = memoryStream.ToArray();
            }

            return Convert.ToBase64String(cipherMessageBytes);
        }

        public static string Decrypt(string cipherText)
        {
            string text = "";
            byte[] key = UTF8Encoding.UTF8.GetBytes(GlobalConstants.KEY);
            byte[] iv = UTF8Encoding.UTF8.GetBytes(GlobalConstants.IV);
            Array.Resize(ref key, 32);
            Array.Resize(ref iv, 16);

            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            Rijndael RijndaelAlg = Rijndael.Create();

            using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, RijndaelAlg.CreateDecryptor(key, iv), CryptoStreamMode.Read))
            {
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                text = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            }

            return text;
        }
    }
}
