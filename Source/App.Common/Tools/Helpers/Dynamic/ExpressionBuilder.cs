﻿using App.Common.Classes.DTO.Request;
using App.Common.Extensions;
using Rollbar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace App.Common.Tools.Helpers
{
    public class ExpressionBuilder
    {
        private static MethodInfo containsMethod
            = typeof(string).GetMethod("Contains",
                new[] { typeof(string) });

        private static MethodInfo startsWithMethod
            = typeof(string).GetMethod("StartsWith",
                new[] { typeof(string) });

        private static MethodInfo endsWithMethod =
            typeof(string).GetMethod("EndsWith",
                new[] { typeof(string) });

        public static Expression<Func<T, bool>> GetExpression<T>(List<FilterParamDTO> filters)
        {

            if (filters == null)
            {
                return null;
            }

            if (filters.Count == 0)
            {
                return null;
            }

            filters = Clearfilters(filters);

            ParameterExpression param = Expression.Parameter(typeof(T), "c");

            Expression exp = null;

            if (filters.Count == 1)
            {
                exp = GetExpression<T>(param, filters[0]);
            }

            else if (filters.Count == 2)
            {
                exp = GetExpression<T>(param, filters[0], filters[1]);
            }

            else
            {
                while (filters.Count > 0)
                {
                    var f1 = filters[0];
                    var f2 = filters[1];

                    if (exp == null)
                    {
                        exp = GetExpression<T>(param, filters[0],
                            filters[1]);
                    }

                    else
                    {
                        exp = Expression.AndAlso(exp,
                            GetExpression<T>(param, filters[0],
                            filters[1]));
                    }

                    filters.Remove(f1);
                    filters.Remove(f2);

                    if (filters.Count == 1)
                    {
                        exp = Expression.AndAlso(exp,
                            GetExpression<T>(param, filters[0]));

                        filters.RemoveAt(0);
                    }
                }
            }

            return exp != null
                ? Expression.Lambda<Func<T, bool>>(exp, param)
                : null;
        }

        private static Expression GetExpression<T>(ParameterExpression param,
            FilterParamDTO filter)
        {
            object value;
            Expression member = null;

            try
            {
                var type = typeof(T);

                member = PropertyExpression.CreateExpression(param,
                    type, filter.PropertyName);

                var targetType = ObjectExtension.IsNullableType(member.Type)
                    ? Nullable.GetUnderlyingType(member.Type)
                    : member.Type;

                value = Convert.ChangeType(filter.Value, targetType);

                if (value.GetType() == typeof(string))
                {
                    value = value.ToSafeLower();
                }

                ConstantExpression constant = Expression.Constant(value);

                if (ObjectExtension.IsNullableType(member.Type))
                {
                    member = Expression.Convert(member, constant.Type);
                }

                switch (filter.Operator)
                {
                    case "Equals":
                        if (value.GetType() == typeof(string))
                        {
                            var expreLower = Expression
                                .Call(member, "ToLower", null);

                            return Expression.Equal(expreLower, constant);
                        }
                        return Expression.Equal(member, constant);

                    case "Contains":
                        var expresionLower = Expression
                            .Call(member, "ToLower", null);

                        return Expression.Call(expresionLower,
                            containsMethod, constant);

                    case "GreaterThan":
                        return Expression.GreaterThan(member, constant);

                    case "GreaterThanOrEqual":
                        return Expression.GreaterThanOrEqual(member,
                            constant);

                    case "LessThan":
                        return Expression.LessThan(member, constant);

                    case "LessThanOrEqualTo":
                        return Expression.LessThanOrEqual(member, constant);

                    case "StartsWith":
                        return Expression.Call(member,
                            startsWithMethod, constant);

                    case "EndsWith":
                        return Expression.Call(member,
                            endsWithMethod, constant);

                    default:
                        return DefaultExpression<T>(param);
                }
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Error(ex);
                return DefaultExpression<T>(param);
            }

        }

        private static BinaryExpression GetExpression<T>(ParameterExpression param, FilterParamDTO filter1, FilterParamDTO filter2)
        {
            Expression result1 = GetExpression<T>(param, filter1);
            Expression result2 = GetExpression<T>(param, filter2);

            return result1 != null && result2 != null ? Expression.AndAlso(result1, result2) : null;
        }

        private static Expression DefaultExpression<T>(ParameterExpression param)
        {
            var mba = PropertyAccesor<T>.Get("Id");
            object value = Convert.ChangeType(1, mba.ReturnType);

            MemberExpression member = Expression.Property(param, "Id");
            ConstantExpression constant = Expression.Constant(value);

            return Expression.GreaterThanOrEqual(member, constant);
        }

        private static List<FilterParamDTO> Clearfilters(List<FilterParamDTO> filters)
        {
            List<FilterParamDTO> resultList = filters;

            for (int i = 0; i < filters.Count; i++)
            {
                var item = filters.ElementAt(i);

                if (string.IsNullOrEmpty(item.Operator) || string.IsNullOrEmpty(item.PropertyName))
                {
                    resultList.RemoveAt(i);
                }
            }

            return resultList;
        }
    }
}
