﻿using System;
using System.Linq.Expressions;

namespace App.Common.Tools.Helpers
{
    public static class PropertyExpression
    {
        public static Expression CreateExpression(Type type,
            string propertyName)
        {
            var param = Expression.Parameter(type, "c");

            Expression body = param;

            foreach (var member in propertyName.Split('.'))
            {
                body = Expression.PropertyOrField(body, member);
            }

            return body;
        }

        public static Expression CreateExpression(ParameterExpression param,
            Type type, string propertyName)
        {
            Expression body = param;
            foreach (var member in propertyName.Split('.'))
            {
                body = Expression.PropertyOrField(body, member);
            }

            return body;
        }
    }
}
