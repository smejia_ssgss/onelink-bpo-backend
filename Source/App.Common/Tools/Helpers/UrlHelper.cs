﻿using Microsoft.AspNetCore.Hosting;

namespace App.Common.Tools.Helpers
{
    public class UrlHelper
    {
        private readonly IHostingEnvironment environment;

        public UrlHelper(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        public bool IsValidRedirectUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }
            else
            {
                return url[0] == '/' && (url.Length == 1
                    || url[1] != '/' && url[1] != '\\')
                    || url.Length > 1 && url[0] == '~' && url[1] == '/';
            }
        }
    }
}