﻿using Newtonsoft.Json;

namespace App.Common.Tools.Helpers
{
    public static class SerializeHelper<T>
    {
        public static string ToJson(T data)
        {
            string result = string.Empty;
            if (data != null)
            {
                result = JsonConvert.SerializeObject(data);
            }

            return result;
        }

        public static T ToEntity(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
