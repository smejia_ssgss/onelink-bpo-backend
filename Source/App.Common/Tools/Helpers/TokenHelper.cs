﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using App.Common.Classes.Constants;
using App.Common.Classes.DTO.Authentications;
using App.Common.Classes.Security;
using Microsoft.IdentityModel.Tokens;

namespace App.Common.Tools.Helpers
{
    public class TokenHelper
    {
        public static SymmetricSecurityKey GetSecurityKey(string key)
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
        }

        public static string GenerateToken(JwtOptions options, InfoUsuarioDTO user, out string cookieHashValue)
        {
            cookieHashValue = GenerateFingerprint();
            var claimValue = GenerateHash(cookieHashValue);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(AuthenticationConstants.FingerprintClaim, claimValue),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                }),
                Expires = options.Expiration,
                SigningCredentials = options.SigningCredentials,
                Issuer = options.Issuer,
                Audience = options.Audience,
                IssuedAt = options.IssuedAt,
                NotBefore = options.NotBefore
            };

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var token = handler.CreateJwtSecurityToken(tokenDescriptor);
            return handler.WriteToken(token);
        }

        public static string GenerateHash(string text)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] bytes = encoding.GetBytes(text);
            byte[] hashbytes;
            using (var hashAlg = SHA512.Create())
            {
                hashbytes = hashAlg.ComputeHash(bytes);
            }

            return BitConverter.ToString(hashbytes).Replace("-", "").ToLower();
        }

        public static string GenerateFingerprint()
        {
            byte[] bytes = new byte[70];
            using (var generator = RandomNumberGenerator.Create())
            {
                generator.GetBytes(bytes);
            }

            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }
    }
}
