﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.Base.Services;
using App.Common.Classes.Cache;
using App.Common.Classes.DTO.Authentications;
using App.Common.Classes.Validator;
using App.Common.Resources;
using App.Control.Services.Contracts;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Control.Services.Classes.Services
{
    public class AutenticacionService : BaseService<UsuarioDTO, Autenticacion>, IAutenticacionService
    {
        private readonly IAutenticacionRepository autenticacionRepository;

        public AutenticacionService(IBaseCRUDRepository<Autenticacion> repository,
            IMemoryCacheManager memoryCache, IMapper mapper, IAutenticacionRepository autenticacionRepository,
            IServiceValidator<Autenticacion> validation, IConfiguration configuration)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
            this.autenticacionRepository = autenticacionRepository;
        }

        public InfoUsuarioDTO Login(UsuarioLoginDTO user)
        {
            return this.autenticacionRepository.Login(user);
        }
    }

    public class AutenticacionValidator : BaseServiceValidator<Autenticacion, AutenticacionResource>
    {
        public AutenticacionValidator(IStringLocalizer<AutenticacionResource> localizer) : base(localizer)
        {
        }

        #region Load Post

        public override void LoadPostUpdateRules()
        {
        }

        public override void LoadPostDeleteRules()
        {
        }

        public override void LoadPostInsertRules()
        {
        }

        #endregion

        #region Load Pre

        public override void LoadPreInsertRules()
        {
        }

        public override void LoadPreUpdateRules()
        {
        }
        public override void LoadPreDeleteRules()
        {
        }

        #endregion
    }
}
