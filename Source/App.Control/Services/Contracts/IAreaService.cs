﻿using App.Common.Classes.Base.Services;
using App.Common.Classes.DTO.Area;

namespace App.Control.Services.Contracts
{
    public interface IAreaService : IBaseService<AreaDTO>
    {
    }
}
