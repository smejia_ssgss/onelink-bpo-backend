﻿using App.Common.Classes.Base.Services;
using App.Common.Classes.DTO.Authentications;

namespace App.Control.Services.Contracts
{
    public interface IAutenticacionService : IBaseService<UsuarioDTO>
    {
        InfoUsuarioDTO Login(UsuarioLoginDTO user);
    }
}
