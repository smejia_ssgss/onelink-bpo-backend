﻿using App.Common.Classes.Base.Services;
using App.Common.Classes.DTO.Empleado;

namespace App.Control.Services.Contracts
{
    public interface IEmpleadoService : IBaseService<EmpleadoDTO>
    {
    }
}
