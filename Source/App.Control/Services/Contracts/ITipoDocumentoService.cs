﻿using App.Common.Classes.Base.Services;
using App.Common.Classes.DTO.TipoDocumento;

namespace App.Control.Services.Contracts
{
    public interface ITipoDocumentoService : IBaseService<TipoDocumentoDTO>
    {
    }
}
