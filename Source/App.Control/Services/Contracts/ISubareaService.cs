﻿using App.Common.Classes.Base.Services;
using App.Common.Classes.DTO.Subarea;
using System.Collections.Generic;

namespace App.Control.Services.Contracts
{
    public interface ISubareaService : IBaseService<SubareaDTO>
    {
        List<SubareaDTO> GetAllByAreaId(int areaId);
    }
}
