﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.Base.Services;
using App.Common.Classes.Cache;
using App.Common.Classes.DTO.Area;
using App.Common.Classes.Validator;
using App.Common.Resources;
using App.Control.Services.Contracts;
using App.Data.Database.Entities;
using AutoMapper;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Control.Services.Classes.Services
{
    public class AreaService : BaseService<AreaDTO, Areas>, IAreaService
    {
        public AreaService(IBaseCRUDRepository<Areas> repository,
            IMemoryCacheManager memoryCache, IMapper mapper,
            IServiceValidator<Areas> validation, IConfiguration configuration)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
        }
    }

    public class AreaValidator : BaseServiceValidator<Areas, AreaResource>
    {
        IStringLocalizer<GlobalResource> _globalResource;

        public AreaValidator(IStringLocalizer<AreaResource> localizer,
            IStringLocalizer<GlobalResource> globalResource) : base(localizer)
        {
            _globalResource = globalResource;
        }

        #region Load Pre

        public override void LoadPreInsertRules()
        {
            PreInsertValidator.RuleFor(t => t.Nombre)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Nombre"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Nombre"));
        }

        public override void LoadPreUpdateRules()
        {
            PreUpdateValidator.RuleFor(t => t.Id)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Id", "cero"));

            PreUpdateValidator.RuleFor(t => t.Nombre)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Nombre"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Nombre"));
        }

        public override void LoadPreDeleteRules()
        {
            PreDeleteValidator.RuleFor(t => t.Id)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Id", "cero"));
        }

        #endregion

        #region Load Post

        public override void LoadPostInsertRules()
        {
        }

        public override void LoadPostUpdateRules()
        {
        }

        public override void LoadPostDeleteRules()
        {
        }

        #endregion
    }
}
