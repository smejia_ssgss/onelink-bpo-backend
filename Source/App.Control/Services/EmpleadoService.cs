﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.Base.Services;
using App.Common.Classes.Cache;
using App.Common.Classes.DTO.Empleado;
using App.Common.Classes.Validator;
using App.Common.Resources;
using App.Control.Services.Contracts;
using App.Data.Database.Entities;
using AutoMapper;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Control.Services.Classes.Services
{
    public class EmpleadoService : BaseService<EmpleadoDTO, Empleados>, IEmpleadoService
    {
        public EmpleadoService(IBaseCRUDRepository<Empleados> repository,
            IMemoryCacheManager memoryCache, IMapper mapper,
            IServiceValidator<Empleados> validation, IConfiguration configuration)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
        }
    }

    public class EmpleadoValidator : BaseServiceValidator<Empleados, EmpleadoResource>
    {
        IStringLocalizer<GlobalResource> _globalResource;

        public EmpleadoValidator(IStringLocalizer<EmpleadoResource> localizer,
            IStringLocalizer<GlobalResource> globalResource)
            : base(localizer)
        {
            _globalResource = globalResource;
        }

        #region Load Pre

        public override void LoadPreInsertRules()
        {
            PreUpdateValidator.RuleFor(t => t.TipoDocumentoId)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Tipo Documento Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Tipo Documento Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Tipo Documento Id", "cero"));

            PreUpdateValidator.RuleFor(t => t.SubareaId)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Subarea Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Subarea Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Subarea Id", "cero"));

            PreInsertValidator.RuleFor(t => t.NumeroDocumento)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Documento"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Documento"));

            PreInsertValidator.RuleFor(t => t.Nombres)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Nombres"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Nombres"));

            PreInsertValidator.RuleFor(t => t.Apellidos)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Apellidos"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Apellidos"));
        }

        public override void LoadPreUpdateRules()
        {
            PreUpdateValidator.RuleFor(t => t.Id)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Id", "cero"));

            PreUpdateValidator.RuleFor(t => t.TipoDocumentoId)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Tipo Documento Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Tipo Documento Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Tipo Documento Id", "cero"));

            PreUpdateValidator.RuleFor(t => t.SubareaId)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Subarea Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Subarea Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Subarea Id", "cero"));

            PreInsertValidator.RuleFor(t => t.NumeroDocumento)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Documento"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Documento"));

            PreInsertValidator.RuleFor(t => t.Nombres)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Nombres"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Nombres"));

            PreInsertValidator.RuleFor(t => t.Apellidos)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Apellidos"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Apellidos"));
        }

        public override void LoadPreDeleteRules()
        {
            PreDeleteValidator.RuleFor(t => t.Id)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Id", "cero"));
        }

        #endregion

        #region Load Post

        public override void LoadPostInsertRules()
        {
        }

        public override void LoadPostUpdateRules()
        {
        }

        public override void LoadPostDeleteRules()
        {
        }

        #endregion
    }
}
