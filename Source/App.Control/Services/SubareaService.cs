﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.Base.Services;
using App.Common.Classes.Cache;
using App.Common.Classes.DTO.Subarea;
using App.Common.Classes.Validator;
using App.Common.Resources;
using App.Control.Services.Contracts;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;
using AutoMapper;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Linq;

namespace App.Control.Services.Classes.Services
{
    public class SubareaService : BaseService<SubareaDTO, Subareas>, ISubareaService
    {
        ISubareaRepository _subareaRepository;

        public SubareaService(IBaseCRUDRepository<Subareas> repository,
            IMemoryCacheManager memoryCache, IMapper mapper,
            IServiceValidator<Subareas> validation, IConfiguration configuration,
            ISubareaRepository subareaRepository)
            : base(repository, memoryCache, mapper, validation, configuration)
        {
            _subareaRepository = subareaRepository;
        }

        public List<SubareaDTO> GetAllByAreaId(int areaId)
        {
            var lista = _subareaRepository.GetAllByAreaId(areaId).ToList();
            return _mapperDependency.Map<List<SubareaDTO>>(lista);
        }
    }

    public class SubareaValidator : BaseServiceValidator<Subareas, SubareaResource>
    {
        IStringLocalizer<GlobalResource> _globalResource;

        public SubareaValidator(IStringLocalizer<SubareaResource> localizer,
            IStringLocalizer<GlobalResource> globalResource) : base(localizer)
        {
            _globalResource = globalResource;
        }

        #region Load Pre

        public override void LoadPreInsertRules()
        {
            PreInsertValidator.RuleFor(s => s.Nombre)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Nombre"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Nombre"));

            PreInsertValidator.RuleFor(s => s.AreaId)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Área Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Área Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Área Id", "cero"));
        }

        public override void LoadPreUpdateRules()
        {
            PreUpdateValidator.RuleFor(t => t.Id)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Id", "cero"));

            PreUpdateValidator.RuleFor(t => t.Nombre)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Nombre"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Nombre"));

            PreUpdateValidator.RuleFor(s => s.AreaId)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Área Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Área Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Área Id", "cero"));
        }

        public override void LoadPreDeleteRules()
        {
            PreDeleteValidator.RuleFor(t => t.Id)
                .NotNull().WithMessage(string.Format(
                    _globalResource["PropertyNotNullMessage"], "Id"))
                .NotEmpty().WithMessage(string.Format(
                    _globalResource["PropertyNotEmptyMessage"], "Id"))
                .GreaterThan(0).WithMessage(string.Format(
                    _globalResource["PropertyGreatherThanMessage"], "Id", "cero"));
        }

        #endregion

        #region Load Post

        public override void LoadPostInsertRules()
        {
        }

        public override void LoadPostUpdateRules()
        {
        }

        public override void LoadPostDeleteRules()
        {
        }

        #endregion
    }
}
