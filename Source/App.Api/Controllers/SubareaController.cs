﻿using App.Common.Classes.Base.WebApi;
using App.Common.Classes.DTO.Subarea;
using App.Common.Resources;
using App.Control.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Web.Controllers.Affiliates
{
    [Route("api/subarea"), Authorize]
    public class SubareaController : BaseController<SubareaDTO>
    {
        protected IConfiguration configuration;
        protected ISubareaService subareaService;
        protected IStringLocalizer<GlobalResource> globalLocalizer;

        public SubareaController(IConfiguration configuration,
            ISubareaService subareaService,
            IStringLocalizer<GlobalResource> globalLocalizer)
            : base(subareaService, globalLocalizer)
        {
            this.configuration = configuration;
            this.subareaService = subareaService;
            this.globalLocalizer = globalLocalizer;
        }

        [HttpGet, Route("getAllByAreaId/{id}")]
        public IActionResult GetAllByAreaId(int id)
        {
            return Ok(subareaService.GetAllByAreaId(id));
        }
    }
}