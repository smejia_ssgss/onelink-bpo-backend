﻿using App.Common.Classes.Base.WebApi;
using App.Common.Classes.DTO.TipoDocumento;
using App.Common.Resources;
using App.Control.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Web.Controllers.Affiliates
{
    [Route("api/tipoDocumento"), Authorize]
    public class TipoDocumentoController : BaseController<TipoDocumentoDTO>
    {
        protected IConfiguration configuration;
        protected ITipoDocumentoService tipoDocumentoService;
        protected IStringLocalizer<GlobalResource> globalLocalizer;

        public TipoDocumentoController(IConfiguration configuration,
            ITipoDocumentoService tipoDocumentoService,
            IStringLocalizer<GlobalResource> globalLocalizer)
            : base(tipoDocumentoService, globalLocalizer)
        {
            this.configuration = configuration;
            this.tipoDocumentoService = tipoDocumentoService;
            this.globalLocalizer = globalLocalizer;
        }
    }
}