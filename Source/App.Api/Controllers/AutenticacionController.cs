﻿using App.Common.Classes.Base.WebApi;
using App.Common.Classes.DTO.Authentications;
using App.Common.Classes.Security;
using App.Common.Resources;
using App.Common.Tools.Helpers;
using App.Control.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace App.Web.Controllers.Affiliates
{
    [Route("api/authentication"), Authorize]
    public class AutenticacionController : BaseController<UsuarioDTO>
    {
        protected IConfiguration configuration;
        protected IAutenticacionService autenticacionService;
        protected IStringLocalizer<GlobalResource> globalLocalizer;

        private readonly JwtOptions jwtOptions;

        public AutenticacionController(IConfiguration configuration,
            IAutenticacionService autenticacionService,
            IStringLocalizer<GlobalResource> globalLocalizer,
            IOptions<JwtOptions> jwtOptions)
            : base(autenticacionService, globalLocalizer)
        {
            this.configuration = configuration;
            this.autenticacionService = autenticacionService;
            this.globalLocalizer = globalLocalizer;
            this.jwtOptions = jwtOptions.Value;
        }

        [HttpPost, Route("login"), AllowAnonymous]
        public virtual IActionResult Login([FromBody] UsuarioLoginDTO loginData)
        {
            if (loginData == null)
            {
                return BadRequest();
            }

            var dataUser = autenticacionService.Login(loginData);
            if (dataUser.Logged)
            {
                dataUser.Token = TokenHelper.GenerateToken(this.jwtOptions,
                    dataUser, out string cookieValue);
            }
            else
            {
                return Unauthorized();
            }

            return Ok(dataUser);
        }

        [HttpPost, Route("logout")]
        public ActionResult Logout()
        {
            return Ok();
        }
    }
}