﻿using App.Common.Classes.Base.WebApi;
using App.Common.Classes.DTO.Empleado;
using App.Common.Resources;
using App.Control.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Web.Controllers.Affiliates
{
    [Route("api/empleado"), Authorize]
    public class EmpleadoController : BaseController<EmpleadoDTO>
    {
        protected IConfiguration configuration;
        protected IEmpleadoService empleadoService;
        protected IStringLocalizer<GlobalResource> globalLocalizer;

        public EmpleadoController(IConfiguration configuration,
            IEmpleadoService empleadoService,
            IStringLocalizer<GlobalResource> globalLocalizer)
            : base(empleadoService, globalLocalizer)
        {
            this.configuration = configuration;
            this.empleadoService = empleadoService;
            this.globalLocalizer = globalLocalizer;
        }
    }
}