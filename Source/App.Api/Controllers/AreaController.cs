﻿using App.Common.Classes.Base.WebApi;
using App.Common.Classes.DTO.Area;
using App.Common.Resources;
using App.Control.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace App.Web.Controllers.Affiliates
{
    [Route("api/area"), Authorize]
    public class AreaController : BaseController<AreaDTO>
    {
        protected IConfiguration configuration;
        protected IAreaService areaService;
        protected IStringLocalizer<GlobalResource> globalLocalizer;

        public AreaController(IConfiguration configuration,
            IAreaService areaService,
            IStringLocalizer<GlobalResource> globalLocalizer)
            : base(areaService, globalLocalizer)
        {
            this.configuration = configuration;
            this.areaService = areaService;
            this.globalLocalizer = globalLocalizer;
        }
    }
}