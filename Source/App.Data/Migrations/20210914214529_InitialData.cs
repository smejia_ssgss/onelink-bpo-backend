﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Data.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Autenticacion] ON");
            migrationBuilder.Sql("INSERT[dbo].[Autenticacion] ([ID], [Usuario], [Contrasena], [Activo]) VALUES(1, N'admin', N'hRg1Y86r9+LJvQZh8lIl3g==', 1)");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Autenticacion] OFF");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[TiposDocumentos] ON");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos] ([ID], [Codigo], [Nombre]) VALUES(1, N'CC', N'CÉDULA DE CIUDADANÍA')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(2, N'CE', N'CÉDULA EXTRANJERÍA')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(3, N'CI', N'CARNET DE IDENTIDAD')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(4, N'DNI', N'DOCUMENTO NACIONAL DE IDENTIDAD')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(5, N'DUI', N'DOCUMENTO ÚNICO DE IDENTIDAD')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(6, N'RC', N'REGISTRO CIVIL')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(7, N'TI', N'TARJETA DE IDENTIDAD')");
            migrationBuilder.Sql("INSERT[dbo].[TiposDocumentos]([ID], [Codigo], [Nombre]) VALUES(8, N'TP', N'TARJETA PASAPORTE')");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[TiposDocumentos] OFF");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Areas] ON");
            migrationBuilder.Sql("INSERT[dbo].[Areas] ([ID], [Nombre]) VALUES(1, N'ADMINISTRACIÓN')");
            migrationBuilder.Sql("INSERT[dbo].[Areas]([ID], [Nombre]) VALUES(2, N'INGENIERÍA')");
            migrationBuilder.Sql("INSERT[dbo].[Areas]([ID], [Nombre]) VALUES(3, N'INVESTIGACIÓN + DESARROLLO')");
            migrationBuilder.Sql("INSERT[dbo].[Areas]([ID], [Nombre]) VALUES(4, N'TALENTO HUMANO')");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Areas] OFF");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Subareas] ON");
            migrationBuilder.Sql("INSERT[dbo].[Subareas] ([ID], [Nombre], [AreaID]) VALUES(1, N'CONTABILIDAD', 1)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(2, N'SEGURIDAD Y SALUD EN EL TRABAJO', 1)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(3, N'SOPORTE TÉCNICO', 2)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(4, N'SEGURIDAD INFORMÁTICA', 2)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(5, N'CALIDAD', 2)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(6, N'INNOVACIÓN', 3)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(7, N'DESARROLLO', 3)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(8, N'BI', 3)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(9, N'BIENESTAR', 4)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(10, N'RECLUTAMIENTO', 4)");
            migrationBuilder.Sql("INSERT[dbo].[Subareas]([ID], [Nombre], [AreaID]) VALUES(11, N'PSICOLOGÍA', 4)");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Subareas] OFF");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Empleados] ON");
            migrationBuilder.Sql("INSERT[dbo].[Empleados] ([ID], [TipoDocumentoID], [NumeroDocumento], [Nombres], [Apellidos], [SubareaID]) VALUES(1, 1, N'10502225050', N'Raúl Alejandro', N'Rodríguez Muñoz', 2)");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[Empleados] OFF");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[TipoRol] ON");
            migrationBuilder.Sql("INSERT[dbo].[TipoRol] ([ID], [EmpleadoID], [AutenticacionID]) VALUES(1, 1, 1)");
            migrationBuilder.Sql("SET IDENTITY_INSERT[dbo].[TipoRol] OFF");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
