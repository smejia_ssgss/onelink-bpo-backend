﻿using App.Data.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace App.Data.Database.Context
{
    public partial class OneLinkBPODbContext : DbContext
    {
        public OneLinkBPODbContext()
        {
        }

        public OneLinkBPODbContext(DbContextOptions<OneLinkBPODbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Autenticacion> Autenticacion { get; set; }
        public virtual DbSet<Empleados> Empleados { get; set; }
        public virtual DbSet<TipoRol> TipoRol { get; set; }
        public virtual DbSet<TiposDocumentos> TiposDocumentos { get; set; }
        public virtual DbSet<Areas> Areas { get; set; }
        public virtual DbSet<Subareas> Subareas { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Autenticacion>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.Activo).IsRequired().HasDefaultValueSql("((1))");
                entity.Property(e => e.Contrasena).IsRequired().IsUnicode(false);
                entity.Property(e => e.Usuario).IsRequired().IsUnicode(false);
            });

            modelBuilder.Entity<Empleados>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("Id");

                entity.Property(e => e.NumeroDocumento)
                    .IsUnicode(false);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.TipoDocumentoId).HasColumnName("TipoDocumentoId");

                entity.Property(e => e.SubareaId).HasColumnName("SubareaId");

                entity.HasOne(d => d.TipoDocumentos)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.TipoDocumentoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empleados_TiposDocumentos");

                entity.HasOne(d => d.Subareas)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.SubareaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empleados_Subareas");
            });

            modelBuilder.Entity<TipoRol>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("Id");

                entity.Property(e => e.AutenticacionId)
                    .HasColumnName("AutenticacionId");

                entity.Property(e => e.EmpleadoId).HasColumnName("EmpleadoId");

                entity.HasOne(d => d.Autenticacion)
                    .WithMany(p => p.TipoRol)
                    .HasForeignKey(d => d.AutenticacionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TipoRol_Autenticacion");

                entity.HasOne(d => d.Empleado)
                    .WithMany(p => p.TipoRol)
                    .HasForeignKey(d => d.EmpleadoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TipoRol_Empleados");
            });

            modelBuilder.Entity<TiposDocumentos>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("Id");

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Areas>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("Id");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Subareas>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("Id");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AreaId).HasColumnName("AreaId");

                entity.HasOne(d => d.Areas)
                    .WithMany(p => p.Subareas)
                    .HasForeignKey(d => d.AreaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subareas_Areas");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
