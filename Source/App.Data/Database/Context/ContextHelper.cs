﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace App.Data.Database.Context
{
    public static class ContextHelper
    {
        /// <returns></returns>
        public static List<T> RawSqlQuery<T>(this OneLinkBPODbContext context, string query, Func<DbDataReader, T> map = null, params SqlParameter[] parameters)
        {
            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                if (parameters?.Length > 0)
                {
                    command.Parameters.AddRange(parameters);
                }

                bool wasOpen = command.Connection.State == ConnectionState.Open;
                if (!wasOpen)
                {
                    command.Connection.Open();
                }

                using (var result = command.ExecuteReader())
                {
                    var entities = new List<T>();

                    if (map != null)
                    {
                        while (result.Read())
                        {
                            entities.Add(map(result));
                        }
                    }
                    else
                    {
                        var properties = typeof(T).GetProperties();
                        while (result.Read())
                        {
                            var instance = (T)Activator.CreateInstance(typeof(T));
                            foreach (var property in properties)
                            {
                                if (result.HasColumn(property.Name))
                                {
                                    TrySetProperty(instance, property.Name, result.GetValue(result.GetOrdinal(property.Name)));
                                }
                            }
                            entities.Add(instance);
                        }
                    }

                    return entities;
                }
            }
        }

        private static bool TrySetProperty(object obj, string property, object value)
        {
            var prop = obj.GetType().GetProperty(property, BindingFlags.Public | BindingFlags.Instance);
            if (prop != null && prop.CanWrite)
            {
                if (value == DBNull.Value)
                {
                    prop.SetValue(obj, null, null);
                }
                else
                {
                    prop.SetValue(obj, value, null);
                }
                return true;
            }
            return false;
        }

        private static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
