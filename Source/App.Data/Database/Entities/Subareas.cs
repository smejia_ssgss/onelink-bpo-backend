﻿using System;
using System.Collections.Generic;

namespace App.Data.Database.Entities
{
    public partial class Subareas
    {
        public Subareas()
        {
            Empleados = new HashSet<Empleados>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int? AreaId { get; set; }

        public Areas Areas { get; set; }
        public virtual ICollection<Empleados> Empleados { get; set; }
    }
}
