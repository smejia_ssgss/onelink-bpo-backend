﻿using System;
using System.Collections.Generic;

namespace App.Data.Database.Entities
{
    public partial class TiposDocumentos
    {
        public TiposDocumentos()
        {
            Empleados = new HashSet<Empleados>();
        }

        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Empleados> Empleados { get; set; }
    }
}
