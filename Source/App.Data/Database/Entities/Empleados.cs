﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Data.Database.Entities
{
    public partial class Empleados
    {
        public Empleados()
        {
            TipoRol = new HashSet<TipoRol>();
        }

        public int Id { get; set; }
        public int? TipoDocumentoId { get; set; }
        public int? SubareaId { get; set; }
        public string NumeroDocumento { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public virtual TiposDocumentos TipoDocumentos { get; set; }
        public virtual Subareas Subareas { get; set; }
        public virtual ICollection<TipoRol> TipoRol { get; set; }
    }
}
