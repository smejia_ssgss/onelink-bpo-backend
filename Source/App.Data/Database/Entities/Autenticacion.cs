﻿using System;
using System.Collections.Generic;

namespace App.Data.Database.Entities
{
    public partial class Autenticacion
    {
        public Autenticacion()
        {
            TipoRol = new HashSet<TipoRol>();
        }

        public int Id { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public bool? Activo { get; set; }

        public virtual ICollection<TipoRol> TipoRol { get; set; }
    }
}
