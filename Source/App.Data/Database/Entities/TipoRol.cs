﻿using System;
using System.Collections.Generic;

namespace App.Data.Database.Entities
{
    public partial class TipoRol
    {
        public int Id { get; set; }
        public int EmpleadoId { get; set; }
        public int AutenticacionId { get; set; }

        public virtual Autenticacion Autenticacion { get; set; }
        public virtual Empleados Empleado { get; set; }
    }
}
