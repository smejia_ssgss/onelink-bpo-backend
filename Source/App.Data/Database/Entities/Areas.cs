﻿using System;
using System.Collections.Generic;

namespace App.Data.Database.Entities
{
    public partial class Areas
    {
        public Areas()
        {
            Subareas = new HashSet<Subareas>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Subareas> Subareas { get; set; }
    }
}
