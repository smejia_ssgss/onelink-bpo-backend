﻿using App.Common.Classes.Base.Repositories;
using App.Data.Database.Context;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace App.Data.Repositories
{
    public class SubareaRepository : BaseCRUDRepository<Subareas>, ISubareaRepository
    {
        public OneLinkBPODbContext Context
        {
            get
            {
                return (OneLinkBPODbContext)_Database;
            }
        }

        public SubareaRepository(OneLinkBPODbContext database)
            : base(database)
        {
        }

        public override IQueryable<Subareas> GetAll()
        {
            return Context.Subareas.Include(c => c.Areas);
        }

        public IQueryable<Subareas> GetAllByAreaId(int areaId)
        {
            return Context.Subareas.Where(c => c.AreaId == areaId);
        }
    }
}
