﻿using App.Common.Classes.Base.Repositories;
using App.Data.Database.Context;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace App.Data.Repositories
{
    public class EmpleadoRepository : BaseCRUDRepository<Empleados>, IEmpleadoRepository
    {
        public OneLinkBPODbContext Context
        {
            get
            {
                return (OneLinkBPODbContext)_Database;
            }
        }

        public EmpleadoRepository(OneLinkBPODbContext database)
            : base(database)
        {
        }

        public override IQueryable<Empleados> GetAll()
        {
            return Context.Empleados.Include(c => c.TipoDocumentos)
                .Include(c => c.Subareas).ThenInclude(c => c.Areas);
        }

        public override Empleados FindById(object id)
        {
            return GetAll().FirstOrDefault(c => c.Id == (int)id);
        }
    }
}
