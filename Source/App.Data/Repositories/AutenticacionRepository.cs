﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.DTO.Authentications;
using App.Common.Tools.Security;
using App.Data.Database.Context;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace App.Data.Repositories
{
    public class AutenticacionRepository : BaseCRUDRepository<Autenticacion>, IAutenticacionRepository
    {
        public OneLinkBPODbContext Context
        {
            get
            {
                return (OneLinkBPODbContext)_Database;
            }
        }

        public AutenticacionRepository(OneLinkBPODbContext database)
            : base(database)
        {
        }

        public InfoUsuarioDTO Login(UsuarioLoginDTO user)
        {
            InfoUsuarioDTO result = new InfoUsuarioDTO() { Logged = false };

            try
            {
                var query = this._Table.Where(a => a.Activo == true && a.Usuario == user.Us).ToList();

                foreach (var userDb in query)
                {
                    string pass = Cryptography.Decrypt(userDb.Contrasena);

                    if (pass == user.Psw)
                    {
                        var accessInfo = Context.TipoRol.Include(c => c.Empleado)
                            .FirstOrDefault(c => c.AutenticacionId == userDb.Id);

                        result.Logged = true;
                        result.Id = userDb.Id;

                        if (accessInfo.EmpleadoId > 0)
                        {
                            result.EmpleadoId = accessInfo.EmpleadoId;
                            result.UserName = $"{accessInfo.Empleado.Nombres}  {accessInfo.Empleado.Apellidos}";
                        }

                        break;
                    }
                }

            }
            catch (Exception ex)
            {
            }

            if (!result.Logged)
                result.Message = "Usuario y/o contraseña incorrectos";

            return result;
        }

        public Autenticacion GetByEmpleadoId(int id)
        {
            return (from autenticacion in this.Context.Autenticacion
                    join tiporol in this.Context.TipoRol on autenticacion.Id
                    equals tiporol.AutenticacionId
                    where tiporol.EmpleadoId == id
                    select autenticacion).Include(a => a.TipoRol).FirstOrDefault();
        }
    }
}
