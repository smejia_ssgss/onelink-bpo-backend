﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.DTO.Authentications;
using App.Data.Database.Entities;

namespace App.Data.Repositories.Contracts
{
    public interface IAutenticacionRepository : IBaseCRUDRepository<Autenticacion>
    {
        InfoUsuarioDTO Login(UsuarioLoginDTO user);
        Autenticacion GetByEmpleadoId(int id);
    }
}
