﻿using App.Common.Classes.Base.Repositories;
using App.Data.Database.Entities;
using System.Linq;

namespace App.Data.Repositories.Contracts
{
    public interface ISubareaRepository : IBaseCRUDRepository<Subareas>
    {
        IQueryable<Subareas> GetAllByAreaId(int areaId);
    }
}
