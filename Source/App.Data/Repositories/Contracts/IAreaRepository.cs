﻿using App.Common.Classes.Base.Repositories;
using App.Data.Database.Entities;

namespace App.Data.Repositories.Contracts
{
    public interface IAreaRepository : IBaseCRUDRepository<Areas>
    {
    }
}
