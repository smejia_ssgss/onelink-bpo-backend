﻿using App.Common.Classes.Base.Repositories;
using App.Data.Database.Context;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;

namespace App.Data.Repositories
{
    public class AreaRepository : BaseCRUDRepository<Areas>, IAreaRepository
    {
        public OneLinkBPODbContext Context
        {
            get
            {
                return (OneLinkBPODbContext)_Database;
            }
        }

        public AreaRepository(OneLinkBPODbContext database)
            : base(database)
        {
        }
    }
}
