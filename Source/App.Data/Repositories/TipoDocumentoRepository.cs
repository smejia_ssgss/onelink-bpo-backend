﻿using App.Common.Classes.Base.Repositories;
using App.Data.Database.Context;
using App.Data.Database.Entities;
using App.Data.Repositories.Contracts;

namespace App.Data.Repositories
{
    public class TipoDocumentoRepository : BaseCRUDRepository<TiposDocumentos>, ITipoDocumentoRepository
    {
        public OneLinkBPODbContext Context
        {
            get
            {
                return (OneLinkBPODbContext)_Database;
            }
        }

        public TipoDocumentoRepository(OneLinkBPODbContext database)
            : base(database)
        {
        }
    }
}
