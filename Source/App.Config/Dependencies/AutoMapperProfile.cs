﻿using App.Common.Classes.DTO.Area;
using App.Common.Classes.DTO.Authentications;
using App.Common.Classes.DTO.Empleado;
using App.Common.Classes.DTO.Subarea;
using App.Common.Classes.DTO.TipoDocumento;
using App.Data.Database.Entities;
using AutoMapper;
using System.Linq;

namespace App.Config.Dependencies
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Empleados, EmpleadoDTO>().ReverseMap();
            CreateMap<TipoDocumentoDTO, TiposDocumentos>().ReverseMap();
            CreateMap<AreaDTO, Areas>().ReverseMap();
            CreateMap<SubareaDTO, Subareas>().ReverseMap();

            CreateMap<Autenticacion, InfoUsuarioSimpleDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Nombre, opt => opt.MapFrom(src => $"{src.TipoRol.FirstOrDefault().Empleado.Nombres} {src.TipoRol.FirstOrDefault().Empleado.Apellidos}"));

            CreateMap<Autenticacion, UsuarioDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Usuario))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Contrasena));

            CreateMap<UsuarioDTO, Autenticacion>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Usuario, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.Contrasena, opt => opt.MapFrom(src => src.Password))
                .ForMember(dest => dest.Activo, opt => opt.MapFrom(src => true));
        }
    }
}
