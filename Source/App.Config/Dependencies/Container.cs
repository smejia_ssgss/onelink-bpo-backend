﻿using App.Common.Classes.Base.Repositories;
using App.Common.Classes.Cache;
using App.Common.Classes.Constants;
using App.Common.Classes.Security;
using App.Common.Classes.Validator;
using App.Common.Tools.Helpers;
using App.Control.Services.Classes.Services;
using App.Control.Services.Contracts;
using App.Data.Database.Context;
using App.Data.Database.Entities;
using App.Data.Repositories;
using App.Data.Repositories.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace App.Config.Dependencies
{
    public class Container
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            #region Initial Settings

            services.AddDbContext<OneLinkBPODbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("Default"), x => x.MigrationsAssembly("App.Data")));

            services.AddMemoryCache();
            services.AddSingleton<IConfiguration>(configuration);

            // AutoMaper
            var configMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
            var mapper = configMapper.CreateMapper();
            services.AddSingleton(mapper);

            // Context
            services.AddScoped<OneLinkBPODbContext, OneLinkBPODbContext>();

            JwtConfigurations(services, configuration);
            services.AddControllers();

            #endregion

            #region Implementations

            #region Repositories

            #region IBaseCRUDRepository

            services.AddScoped<IBaseCRUDRepository<Areas>, AreaRepository>();
            services.AddScoped<IBaseCRUDRepository<Autenticacion>, AutenticacionRepository>();
            services.AddScoped<IBaseCRUDRepository<Empleados>, EmpleadoRepository>();
            services.AddScoped<IBaseCRUDRepository<Subareas>, SubareaRepository>();
            services.AddScoped<IBaseCRUDRepository<TiposDocumentos>, TipoDocumentoRepository>();
            services.AddScoped<IBaseCRUDRepository<TipoRol>, TipoRolRepository>();

            #endregion

            #region Contracts

            services.AddScoped<IAreaRepository, AreaRepository>();
            services.AddScoped<IAutenticacionRepository, AutenticacionRepository>();
            services.AddScoped<IEmpleadoRepository, EmpleadoRepository>();
            services.AddScoped<ISubareaRepository, SubareaRepository>();
            services.AddScoped<ITipoDocumentoRepository, TipoDocumentoRepository>();
            services.AddScoped<ITipoRolRepository, TipoRolRepository>();

            #endregion

            #endregion

            #region Services

            services.AddScoped<IAreaService, AreaService>();
            services.AddScoped<IAutenticacionService, AutenticacionService>();
            services.AddScoped<IEmpleadoService, EmpleadoService>();
            services.AddScoped<ISubareaService, SubareaService>();
            services.AddScoped<ITipoDocumentoService, TipoDocumentoService>();

            #endregion

            #region Validators

            services.AddScoped<IServiceValidator<Areas>, AreaValidator>();
            services.AddScoped<IServiceValidator<Autenticacion>, AutenticacionValidator>();
            services.AddScoped<IServiceValidator<Empleados>, EmpleadoValidator>();
            services.AddScoped<IServiceValidator<Subareas>, SubareaValidator>();
            services.AddScoped<IServiceValidator<TiposDocumentos>, TipoDocumentoValidator>();

            #endregion

            #region Others

            services.AddSingleton<IMemoryCacheManager, MemoryCacheManager>();

            var sites = configuration.GetSection("Cors").GetSection("Sites").Get<string[]>();
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.WithOrigins(sites)
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "OneLinkBPO",
                    Description = "OneLinkBPO",
                    Version = "v1"
                });
            });

            #endregion

            #endregion

            #region Additional Settings

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Configure<RequestLocalizationOptions>(opts =>
            {
                string english = "en-US";
                string spanish = "es";
                string spanishColombia = "es-CO";

                var supportedCultures = new List<CultureInfo> {
                    new CultureInfo(english),
                    new CultureInfo(spanish),
                    new CultureInfo(spanishColombia)
                };
                opts.DefaultRequestCulture = new RequestCulture(culture: english, uiCulture: english);
                opts.SupportedCultures = supportedCultures;
                opts.SupportedUICultures = supportedCultures;
            });

            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization();

            #endregion
        }

        private static void JwtConfigurations(IServiceCollection services, IConfiguration configuration)
        {
            var jwtoptions = configuration.GetSection(AuthenticationConstants.SectionName);
            var key = TokenHelper.GetSecurityKey(jwtoptions[AuthenticationConstants.Key]);

            services.Configure<JwtOptions>(options =>
            {
                options.Issuer = jwtoptions[AuthenticationConstants.Issuer];
                options.Audience = jwtoptions[AuthenticationConstants.Audience];
                options.SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(configOptions =>
            {
                configOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = jwtoptions[AuthenticationConstants.Issuer],
                    ValidateAudience = true,
                    ValidAudience = jwtoptions[AuthenticationConstants.Audience],
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = key,
                    RequireExpirationTime = true,
                    ValidateLifetime = true
                };
                configOptions.SaveToken = true;
                configOptions.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        context.Success();
                        return Task.CompletedTask;
                    }
                };
            });
        }
    }
}
